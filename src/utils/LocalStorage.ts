import { AsyncStorage } from 'react-native';
import api from '../services/api';
import { Teacher } from '../components/TeacherItem/index';
import baseInterface from './baseInterface';

/*
 * Deals with the local storage of Notes into AsyncStorage
 *
 * @class LocalStorage
 */

class LocalStorage<T> {

    async getItems(dataId: string): Promise<Array<T>> {

        let json: string | null = await AsyncStorage.getItem(`@data:${dataId}`)

        return JSON.parse(json || "[]") as Array<T>;
    }

    async setItem(dataId: string, newItem: baseInterface): Promise<void> {

        let items = await this.getItems(dataId);
        return AsyncStorage.setItem(`@data:${dataId}`, JSON.stringify([...items, newItem]));

    }

    async removeItem(dataId: string, itemArg: T) {

        let items = await this.getItems(dataId);

        items = items.filter((item)=>{
            return item.id !== itemArg.id;
        });

        AsyncStorage.setItem(`@data:${dataId}`, JSON.stringify(items));

    }

};

export const dataSet ={
    Favorites: 'Favorites',
}

const localStorage = new LocalStorage<Teacher>();
export default localStorage;