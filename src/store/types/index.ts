import { Teacher } from "../../components/TeacherItem";

export const favoriteTypes = {
    SET_FAVORITE:    'SET_RESPONSE',
    ADD_FAVORITE:    'ADD_FAVORITE',
    REMOVE_FAVORITE: 'REMOVE_FAVORITE'
};

export type Action = {type: string, payload: Teacher | Teacher[]}
