import { Teacher } from "../../components/TeacherItem";
import { Action, favoriteTypes } from "../types";
import localSorage from "../../utils/LocalStorage";

export interface favoritesState {
    favorites: Teacher[];
}

const initialState = {
    favorites: [],
}

const favoriteItems = (state: favoritesState = initialState, action: Action) => {
    switch(action.type){
        case favoriteTypes.SET_FAVORITE:
            return {...state, favorites: action.payload};
        case favoriteTypes.ADD_FAVORITE:
            return {...state, favorites: action.payload};
        case favoriteTypes.REMOVE_FAVORITE:
            return {...state, favorites: state.favorites.filter(favoriteItem => favoriteItem.id !== action.payload.id)};
    }
    return state;
}

export default favoriteItems;