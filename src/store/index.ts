import { createStore } from 'redux';
import favoriteItems from './reducers/favoriteItems';

export const store = createStore(favoriteItems);