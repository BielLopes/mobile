import { Action, favoriteTypes } from "../types";
import { Teacher } from "../../components/TeacherItem";



export const setFavorite = (teachers: Teacher[]): Action => ({
    type: favoriteTypes.SET_FAVORITE,
    payload: teachers,
})

export const addFavorite = (teacher: Teacher): Action => ({
    type: favoriteTypes.ADD_FAVORITE,
    payload: teacher,
})

export const removeFavorite = (teacher: Teacher): Action => ({
    type: favoriteTypes.REMOVE_FAVORITE,
    payload: teacher,
})