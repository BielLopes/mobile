import axios from 'axios'

const endPoint = 'http://192.168.18.67:3333';

const api = axios.create({
    baseURL: endPoint,
})
//Sempre usar o http:// ao colocar uma url
export default api;