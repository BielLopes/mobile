import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import LandingScreen from '../pages/LandingScreen'
import GiveClassesScreen from '../pages/GiveClassesScreen'
import StudyTabs from './StudyTabs'

const  { Navigator, Screen } = createStackNavigator();

const AppStack = () => {
    return(
        <NavigationContainer>
            <Navigator screenOptions={{ headerShown: false }} >
                <Screen name="Landing" component={LandingScreen} />
                <Screen name="GiveClasses" component={GiveClassesScreen} />
                <Screen name="Study" component={StudyTabs} />
            </Navigator>
        </NavigationContainer>
    );
}

export default AppStack;