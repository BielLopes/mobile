import React from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { RectButton } from 'react-native-gesture-handler'

import styles from './styles'

import landingImg from '../../assets/images/landing.png'
import studyIcon from '../../assets/images/icons/study.png'
import giveClassesIcon from '../../assets/images/icons/give-classes.png'
import heartIcon from '../../assets/images/icons/heart.png'

import { useNavigation } from '@react-navigation/native'

const LandingScreen = () => {

    const navigation = useNavigation();
    

    const handleNavigationToGiveClassesPage = () => {
        navigation.navigate('GiveClasses');
    }

    const handleNavigationToStudyPages = () => {
        navigation.navigate('Study')
    }


    return (
        <View style={styles.container} >
            <Image 
                source={landingImg} 
                style={styles.banner}
            />
            <Text style={styles.title} >
                Seja bem-vindo {'\n'}
                <Text style={styles.titleBold} >O que deseja fazer?</Text>
            </Text>
            <View style={styles.buttonsContainer} >
                <RectButton 
                    style={[styles.button, styles.buttonPrimary]} 
                    onPress={handleNavigationToStudyPages}
                >
                    <Image source={studyIcon} />
                    <Text style={styles.buttonText} >Estudar</Text>
                </RectButton>

                <RectButton 
                    onPress={handleNavigationToGiveClassesPage} 
                    style={[styles.button, styles.buttonSecundary]} 
                >
                    <Image source={giveClassesIcon} />
                    <Text style={styles.buttonText} >Dar Aulas</Text>
                </RectButton>
            </View>

            <Text style={styles.totalConnections} >
                Total de 285 conexões já realizadas {'  '}
                <Image source={heartIcon} />
            </Text>
            
        </View>
    );
}

export default LandingScreen;