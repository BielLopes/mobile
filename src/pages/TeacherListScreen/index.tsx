import React, { useState, useEffect } from 'react'
import { View, ScrollView, Text, TextInput, Image, Picker } from 'react-native';
import PageHeader from '../../components/PageHeader';
import { BorderlessButton, RectButton } from 'react-native-gesture-handler';
import { Feather } from '@expo/vector-icons'
import TeacherItem, { Teacher } from '../../components/TeacherItem';
import { connect, ConnectedProps } from 'react-redux';

import styles from './styles';

import api from '../../services/api';
import localStorage, { dataSet } from '../../utils/LocalStorage'
import { hours, weekDays, subjects } from '../../utils/formFilterData';
import { removeFavorite, addFavorite, setFavorite } from '../../store/actions/favoritesActions';
import { favoritesState } from '../../store/reducers/favoriteItems';
import { useNavigation } from '@react-navigation/native';

const mapStateToProps = (state: favoritesState) => {
    return{
      favoritesItems: state.favorites
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return{
      onSetFavorites: (teachers: Teacher[]) => dispatch(setFavorite(teachers)),
      onAddFavorites: (teacher: Teacher) => dispatch(addFavorite(teacher)),
      onRemoveFavorites: (teacher: Teacher) => dispatch(removeFavorite(teacher)),
    
    }
}


const connector = connect(mapStateToProps, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux;

const TeacherListScreen = (props: Props) => {

    const [isFiltersVisible, setIsfilterVisible] = useState<boolean>(false);
    const [teachers, setTeachers] = useState<Teacher[]>([]);

    const [subject, setSubject] = useState(subjects[0].value);
    const [weekDay, setWeekDay] = useState(weekDays[1].value);
    const [time, setTime] = useState(hours[10].value);    

    const handleToggleFilterVisible = () => {
        setIsfilterVisible(!isFiltersVisible);
    }

    const handleFavoriteClasses = async (teacherArg: Teacher) => {

        console.log(teacherArg);
        if(teacherArg.isFavorited){
            localStorage.removeItem(dataSet.Favorites, teacherArg);
            props.onRemoveFavorites(teacherArg);
        }else{
            localStorage.setItem(dataSet.Favorites, teacherArg);
            const newFavorites = props.favoritesItems;
            newFavorites.push(teacherArg);
            props.onSetFavorites(newFavorites);
        }

    }

    const navigation = useNavigation();

    const handleAllClasses = async () => {
   
        const res = await api.get('/classes/all');
        const favorites = await localStorage.getItems(dataSet.Favorites);

        favorites.map((favorite) => { favorite.isFavorited = true });
        props.onSetFavorites(favorites);

        let allTeachers = res.data;
        
        allTeachers.map((teacher: Teacher)=>{
            
            teacher.isFavorited = false;

            favorites.map((favorite: Teacher) => {
                if(teacher.id === favorite.id){
                    teacher.isFavorited = true;
                }
            });

        });

        setTeachers(allTeachers);
    
    }


    useEffect(() => {
        //console.log("Tamanho da lista de favoritos da perspectiva de TeacherList: ");
        //console.log(props.favoritesItems.length);

        if(teachers.length){

            const NewAllTeachers = teachers;
        
            NewAllTeachers.map((teacher: Teacher)=>{
            
                teacher.isFavorited = false;

                props.favoritesItems.map((favorite: Teacher) => {
                    if(teacher.id === favorite.id){
                        teacher.isFavorited = true;
                    }
                });

            });

            setTeachers(NewAllTeachers);
            //console.log("Certamente mudei o estado dos teacher da Screen de favorite Items: ", teachers);

        }

    },[props.favoritesItems]);

    const searchTeachers = async () => {


        const res = await api.get('/classes',{
            params: {
                subject,
                week_day: weekDay,
                time,
            }
        });

        let allTeachers = res.data;
        
        allTeachers.map((teacher: Teacher)=>{
            
            teacher.isFavorited = false;

            props.favoritesItems.map((favorite: Teacher) => {
                if(teacher.id === favorite.id){
                    teacher.isFavorited = true;
                }
            });

        });
        setTeachers(allTeachers);
        console.log("Mandei atualizar Teachers")
    }

    /*
    useEffect(() => {
        console.log(teachers);
    },[teachers])
    */
    useEffect( () => {
        handleAllClasses();
    },[]);

    return(

        <View style={styles.container} >
            
            <PageHeader 
                title="Proffys disponíveis" 
                headerRight={(
                    <BorderlessButton onPress={handleToggleFilterVisible} >
                        <Feather name="filter" size={20} color="#FFF" />
                    </BorderlessButton>
                )} 
            >

                { isFiltersVisible && (
                    <View style={styles.searchForm} >
                        <Text style={styles.label} > Matéria </Text>
                        <Picker
                            selectedValue={subject}
                            style={styles.input}
                            onValueChange={(itemValue, itemIndex) => setSubject(itemValue)}
                        >
                                {subjects.map(diciplin => <Picker.Item key={diciplin.id} label={diciplin.value} value={diciplin.value} />)}
                        </ Picker>
                        <View style={styles.inputGroup} >
    
                            <View style={styles.inputBlock} >
                                <Text style={styles.label} > Dia da semana </Text>
                                
                                <Picker
                                selectedValue={weekDay}
                                style={styles.input}
                                onValueChange={(itemValue, itemIndex) => setWeekDay(itemValue)}
                                >
                                    {weekDays.map(day => <Picker.Item key={day.id} label={day.value} value={day.id} />)}
                                </ Picker>

                            </View>

                            <View style={styles.inputBlock} >
                                <Text style={styles.label} > Horário </Text>

                                <Picker
                                selectedValue={time}
                                style={styles.input}
                                onValueChange={(itemValue, itemIndex) => setTime(itemValue)}
                                >
                                    {hours.map(hour => <Picker.Item key={hour.id} label={hour.value} value={hour.value} />)}
                                </ Picker>

                            </View>

                        </View>

                        <RectButton 
                            style={styles.submitButton} 
                            onPress={searchTeachers}
                        >
                            <Text style={styles.submitButtonText} >Filtrar</Text>
                        </RectButton>
                    </View>
                )}
            </PageHeader>

            <ScrollView 
                style= {styles.teacherList}
                contentContainerStyle={{
                    paddingHorizontal: 16,
                    paddingBottom: 18,
                }}
            >
                {
                    teachers.map((teacher: Teacher, index) => {
                        return(
                            <View key={teacher.id} >
                            <TeacherItem 
                                
                                teacher={teacher}
                                like={handleFavoriteClasses}
                            />
                            <Text>  {teacher.isFavorited ? "Aloha":"chau"  } </Text>
                            </View>
                        );
                    })
                }
                
            </ScrollView>    
        </View>
    );
}

//Usar o contentContainerStyle é melhor para aplicar padding

export default connector(TeacherListScreen);