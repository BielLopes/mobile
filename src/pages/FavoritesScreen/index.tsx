import React, { useEffect, useState } from 'react'
import { View, ScrollView } from 'react-native';
import PageHeader from '../../components/PageHeader';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch, connect, ConnectedProps } from 'react-redux';

import styles from './styles'
import TeacherItem, { Teacher } from '../../components/TeacherItem';
import localStorage, { dataSet } from '../../utils/LocalStorage';
import { favoritesState } from '../../store/reducers/favoriteItems';
import { addFavorite, setFavorite, removeFavorite } from '../../store/actions/favoritesActions';

const mapStateToProps = (state: favoritesState) => {
    return{
      favoritesItems: state.favorites
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return{
      onSetFavorites: (teachers: Teacher[]) => dispatch(setFavorite(teachers)),
      onRemoveFavorites: (teacher: Teacher) => dispatch(removeFavorite(teacher)),
    
    }
}


const connector = connect(mapStateToProps, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof connector>

type Props = PropsFromRedux;

const  FavoritesScreen: React.FC<Props> =  (props: Props) => { 

    //const favoritesStore = useSelector<favoritesState, Teacher>((state) => state.favorites);
    // O primeiro parametro qual a interface do state e o segundo o tipo de retorno do useSelector

    const navigation = useNavigation();
    const [teachers, setTeachers] = useState<Teacher[]>([]);

    useEffect(() => {
        //console.log("Tamanho da lista de favoritos da perspequitiva da página de Favoritos");
        //console.log(props.favoritesItems.length);

        setTeachers(props.favoritesItems);
    },[props.favoritesItems]);

    /*
    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            
            //const favorites = await localStorage.getItems(dataSet.Favorites);
            const favorites = props.favoritesItems;

            
            props.onSetFavorites(favorites);
            //console.log(props.favoritesItems.length);
            
            setTeachers(favorites);
        });
    
        return unsubscribe;
    },[navigation]);
    */

    const handleAllFavorites = async () => {
   
        const favorites = await localStorage.getItems(dataSet.Favorites);
        favorites.map((favorite) => {
            favorite.isFavorited = true;
        })
        setTeachers(favorites);
        props.onSetFavorites(favorites);
        //console.log(props.favoritesItems);
        
    }

    const handleFavoriteClasses = async (teacherArg: Teacher) => {
        
        localStorage.removeItem(dataSet.Favorites, teacherArg);
        props.onRemoveFavorites(teacherArg);

    }

    useEffect( () => {
        handleAllFavorites();
    },[]);

    return(
        <View style={styles.container} >
            <PageHeader title="Meus proffys favoritos" />

            <ScrollView 
                style= {styles.teacherList}
                contentContainerStyle={{
                    paddingHorizontal: 16,
                    paddingBottom: 18,
                }}
            >
                {
                    teachers.map((teacher: Teacher, index) => {
                        return(
                            <TeacherItem 
                                key={teacher.id} 
                                teacher={teacher}
                                like={handleFavoriteClasses}
                            />
                        );
                    })
                }

            </ScrollView> 
        </View>
    );
}

export default connector(FavoritesScreen);