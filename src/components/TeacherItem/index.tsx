import React, { useState, useEffect } from 'react'
import { View, Image, Text } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import baseInterface from '../../utils/baseInterface';

import styles from './styles';
import heartOutlineIcon from '../../assets/images/icons/heart-outline.png';
import unfavoriteIcon from '../../assets/images/icons/unfavorite.png';
import whatsAppIcon from '../../assets/images/icons/whatsapp.png';

export interface Teacher extends baseInterface {
    avatar: string;
    bio: string;
    cost: number;
    name: string;
    subject: string;
    whatsapp: string;
    isFavorited: boolean; 
}

interface TeacherItemProps{
    teacher: Teacher;
    like: (teacher: Teacher)=> Promise<void>;
}

const TeacherItem: React.FC<TeacherItemProps> = ({ teacher, like }) => {

    
    //const [isFavorite, setIsFavorite] = useState(teacher.isFavorited);

    
    useEffect(()=>{
        console.log("Alterou-se o valor de favorito!!!!!!!!!!");
        //setIsFavorite(teacher.isFavorited);
        console.log(teacher.isFavorited);
    }, [teacher.isFavorited])
    

    return(
        <View style={styles.container} >
            <View style={styles.profile} >
                <Image
                    style={styles.avatar}
                    source={{uri: teacher.avatar}}
                /> 
                <View style={styles.profileInfo} >
                    <Text style={styles.name} >
                        {teacher.name}
                    </Text>
                    <Text style={styles.subject} >
                        {teacher.subject}
                    </Text>
                </View>

            </View>
            <Text style={styles.bio} >
                Fica viajando nas equações diferenciais parciais.
                {'\n'}{'\n'}
                {teacher.bio}
            </Text>

            <View style={styles.footer} >
                <Text style={styles.price} >
                    Preço/hora {'   '} 
                    <Text style={styles.priceValue} >
                        £ {teacher.cost}
                    </Text>
                </Text>

                <View style={styles.buttonsContainer} >
                    <RectButton 
                        style={[styles.favoriteButton, teacher.isFavorited? styles.favorited : null]} 
                        onPress={() => like(teacher)}
                    >
                        <Image source={teacher.isFavorited ? unfavoriteIcon: heartOutlineIcon} />
                    </RectButton>

                    <RectButton style={styles.contactButton} >
                        <Image source={whatsAppIcon} />
                        <Text style={styles.contactButtonText} >
                            Entrar em contato
                        </Text>
                    </RectButton>
                </View>
            </View>
        </View>
    );
}

export default TeacherItem;

//trivago, decolar, goll -> layouts para o app, smiles
// o end point chama flight