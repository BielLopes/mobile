import { StatusBar } from 'expo-status-bar';
import React from 'react';

import { AppLoading } from 'expo';
import {
  useFonts,
  Archivo_400Regular,
  Archivo_700Bold,
} from '@expo-google-fonts/archivo';
import {
  Poppins_400Regular,
  Poppins_600SemiBold,
} from '@expo-google-fonts/poppins';
import AppStack from './src/routes/AppStack';

import { Provider } from 'react-redux';
import { store } from './src/store'


export default function App() {
  
  let [fontsLoaded] = useFonts({
    Archivo_400Regular,
    Archivo_700Bold,
    Poppins_400Regular,
    Poppins_600SemiBold,

  });

  if(!fontsLoaded){
    return <AppLoading />
  }else {
    return (
      <Provider store={store}>
        <AppStack />
        <StatusBar style="light" />
      </Provider>
    );
  }

  
}
// O <> </> é chamado Fragment
//React native não herda estilo, menos o Text
//A flexDirection Padrão do flex é colum, pois aproveita melhor o espaço disponível nos celulares
//Todo component tem display flex por default

// comad + D - Abre o menu de desenvolvedor  |  comad + M - para Android

